﻿using Accord.MachineLearning.Bayes;
using Accord.MachineLearning.DecisionTrees;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.Statistics.Analysis;
using Accord.Statistics.Distributions.Fitting;
using Accord.Statistics.Distributions.Univariate;
using Accord.Statistics.Kernels;
using Accord.Statistics.Models.Regression.Linear;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

namespace EPLClasifier
{
    class Analysis
    {
        public List<Fixture> fixtures = new List<Fixture>();
        public double[][] standartizedInputsData;
        public int[][] standartizedInputsIntegerData;
        public int[] standartizedResultsData;
        public double[] svmAccuracies;
        public double[] bayesAccuracies;
        public double[] randomForestAccuracies;
        public double[] votingAccuracies;
        private TextBox textBox;

        public Analysis(TextBox textBox)
        {
            this.textBox = textBox;
        }

        public void ComputePCA()
        {
            double[][] data = new double[fixtures.Count][];
            int[] outputs = new int[fixtures.Count];
            for (int i = 0; i < fixtures.Count; i++)
            {
                data[i] = new double[fixtures[i].GetAttributesCount()];
                for (int j = 0; j < fixtures[i].GetAttributesCount(); j++)
                {
                    data[i][j] = fixtures[i].GetAttributeValueAtIndex(j);
                }
                if (fixtures[i].fullTimeResult.Equals("H"))
                {
                    outputs[i] = 0;
                }
                else if(fixtures[i].fullTimeResult.Equals("D"))
                {
                    outputs[i] = 1;
                }
                else
                {
                    outputs[i] = 2;
                }
            }

            //Esminių komponenčių analizė
            var pca = new PrincipalComponentAnalysis()
            {
                Method = PrincipalComponentMethod.Center,
                //Balinimo tikslas padaryti įvesčių reikšmingesnes
                Whiten = true
            };
            //Iš duomenų gauname tiesinę regresiją
            MultivariateLinearRegression transform = pca.Learn(data);
            //dimensijų nemažinant gauti duomenys
            double[][] inputs1 = pca.Transform(data);
            //bent 80% paaiškintos dispersijos taisykle gauta sumažinto dydžio dvimatis masyvas
            // turi paimti tiek komponentų, kad jų bendras prisidėjimas būtų > 80%
            pca.ExplainedVariance = 0.8;
            double[][] inputs2 = pca.Transform(data);
            pca.ExplainedVariance = 0.9;
            double[][] inputs3 = pca.Transform(data);
            pca.ExplainedVariance = 0.7;
            double[][] inputs4 = pca.Transform(data);

            //TODO ištęstuoti gautus duomenis su modeliais
            Debug.WriteLine("Nemažintos dimensijos");
            TestInputsOutputs(inputs1, outputs);
            Debug.WriteLine("Mažintos dimensijos 0,8");
            TestInputsOutputs(inputs2, outputs);
            Debug.WriteLine("Mažintos dimensijos 0,9");
            TestInputsOutputs(inputs3, outputs);
            Debug.WriteLine("Mažintos dimensijos 0,7");
            TestInputsOutputs(inputs4, outputs);
        }

        private void TestInputsOutputs(double[][] inputs, int[] outputs) 
        {
            int parts = 10;
            var partCount = inputs.Length / parts;
            double[][] trainingInputs = new double[inputs.Length - partCount][];
            int[] trainingOutputs = new int[outputs.Length - partCount];
            double[][] testingInputs = new double[partCount][];
            int[] testingOutputs = new int[partCount];
            svmAccuracies = new double[parts];
            bayesAccuracies = new double[parts];
            randomForestAccuracies = new double[parts];
            votingAccuracies = new double[parts];

            for (int i = 0; i < parts; i++)
            {
                GetTestingTrainingData(i, partCount, testingInputs, testingOutputs, trainingInputs, trainingOutputs);
                // Atsitiktinio miško metodas
                int[] predictedByRandomForest = RandomForest(trainingInputs, trainingOutputs, testingInputs);

                //Atraminiu vektoriu metodas
                int[] predictedBySvm = SVM(trainingInputs, trainingOutputs, testingInputs);

                // Bajeso metodas
                int[] predictedByBayes = Bayes(trainingInputs, trainingOutputs, testingInputs);
                
                for (int k = 0; k < testingOutputs.Length; k++)
                {
                    int votedPrediction = VotingClassifier(predictedByRandomForest[k], predictedBySvm[k], predictedByBayes[k]);
                    if (votedPrediction == testingOutputs[k])
                    {
                        votingAccuracies[i]++;
                    }
                    if (predictedByBayes[k] == testingOutputs[k])
                    {
                        bayesAccuracies[i]++;
                    }
                    if (predictedByRandomForest[k] == testingOutputs[k])
                    {
                        randomForestAccuracies[i]++;
                    }
                    if (predictedBySvm[k] == testingOutputs[k])
                    {
                        svmAccuracies[i]++;
                    }
                }
                votingAccuracies[i] = votingAccuracies[i] / testingOutputs.Length;
                bayesAccuracies[i] = bayesAccuracies[i] / testingOutputs.Length;
                randomForestAccuracies[i] = randomForestAccuracies[i] / testingOutputs.Length;
                svmAccuracies[i] = svmAccuracies[i] / testingOutputs.Length;
            }
            Debug.WriteLine("TestInputsOutputs end");
            PrintAccuracies();
        }

        public void Calculate()
        {
            if (standartizedInputsData.Length == 0 || standartizedResultsData.Length == 0)
            {
                throw new Exception("Nėra standartizuotų duomenų atilkti operacijai");
            }

            int parts = 10;
            var partCount = standartizedInputsData.Length / parts;
            double[][] trainingInputs = new double[standartizedInputsData.Length - partCount][];
            int[] trainingOutputs = new int[standartizedInputsData.Length - partCount];
            double[][] testingInputs = new double[partCount][];
            int[] testingOutputs = new int[partCount];
            svmAccuracies = new double[parts];
            bayesAccuracies = new double[parts];
            randomForestAccuracies = new double[parts];
            votingAccuracies = new double[parts];

            for (int i = 0; i < parts; i++)
            {
                GetTestingTrainingData(i, partCount, testingInputs, testingOutputs, trainingInputs, trainingOutputs);
                // Atsitiktinio miško metodas
                int[] predictedByRandomForest = RandomForest(trainingInputs, trainingOutputs, testingInputs);

                //Atraminiu vektoriu metodas
                int[] predictedBySvm = SVM(trainingInputs, trainingOutputs, testingInputs);

                // Bajeso metodas
                int[] predictedByBayes = Bayes(trainingInputs, trainingOutputs, testingInputs);
                
                for (int k = 0; k < testingOutputs.Length; k++)
                {
                    int votedPrediction = VotingClassifier(predictedByRandomForest[k], predictedBySvm[k], predictedByBayes[k]);
                    if (votedPrediction == testingOutputs[k])
                    {
                        votingAccuracies[i]++;
                    }
                    if (predictedByBayes[k] == testingOutputs[k])
                    {
                        bayesAccuracies[i]++;
                    }
                    if (predictedByRandomForest[k] == testingOutputs[k])
                    {
                        randomForestAccuracies[i]++;
                    }
                    if (predictedBySvm[k] == testingOutputs[k])
                    {
                        svmAccuracies[i]++;
                    }
                }
                votingAccuracies[i] = votingAccuracies[i] / testingOutputs.Length;
                bayesAccuracies[i] = bayesAccuracies[i] / testingOutputs.Length;
                randomForestAccuracies[i] = randomForestAccuracies[i] / testingOutputs.Length;
                svmAccuracies[i] = svmAccuracies[i] / testingOutputs.Length;
                textBox.Text += String.Format("{0}%", i * parts);
            }
            Debug.WriteLine("Calculate end");
            PrintAccuracies();
        }

        public void PrintAccuracies()
        {
            double sum = 0.0;
            textBox.Text = "";
            foreach (double accuracy in votingAccuracies)
            {
                Debug.WriteLine("Voting accuracy {0:F4}", accuracy);
                textBox.Text += String.Format("Voting accuracy {0:F4}\n", accuracy);
                sum += accuracy;
            }
            textBox.Text += String.Format("Overall voting accuracy {0:F4}\n", sum / votingAccuracies.Length);
            Debug.WriteLine("Overall voting accuracy {0:F4}", sum / votingAccuracies.Length);

            sum = 0.0;
            foreach (double accuracy in bayesAccuracies)
            {
                Debug.WriteLine("Bayes accuracy {0:F4}", accuracy);
                textBox.Text += String.Format("Bayes accuracy {0:F4}\n", accuracy);
                sum += accuracy;
            }
            textBox.Text += String.Format("Overall Bayes accuracy {0:F4}\n", sum / bayesAccuracies.Length);
            Debug.WriteLine("Overall Bayes accuracy {0:F4}", sum / bayesAccuracies.Length);

            sum = 0.0;
            foreach (double accuracy in randomForestAccuracies)
            {
                Debug.WriteLine("Random forests accuracy {0:F4}", accuracy);
                textBox.Text += String.Format("Random forests accuracy {0:F4}\n", accuracy);
                sum += accuracy;
            }
            textBox.Text += String.Format("Overall random forests accuracy {0:F4}\n", sum / randomForestAccuracies.Length);
            Debug.WriteLine("Overall random forests accuracy {0:F4}", sum / randomForestAccuracies.Length);

            sum = 0.0;
            foreach (double accuracy in svmAccuracies)
            {
                Debug.WriteLine("SVM accuracy {0:F4}", accuracy);
                textBox.Text += String.Format("SVM accuracy {0:F4}\n", accuracy);
                sum += accuracy;
            }
            textBox.Text += String.Format("Overall SVM accuracy {0:F4}\n", sum / svmAccuracies.Length);
            Debug.WriteLine("Overall SVM accuracy {0:F4}", sum / svmAccuracies.Length);
        }

        public int VotingClassifier(int p1, int p2, int p3)
        {
            if (p1 == p2)
            {
                return p1;
            }
            else if (p2 == p3)
            {
                return p2;
            }
            else if (p1 == p3)
            {
                return p1;
            }
            return 1;
        }

        public void StandartizeData()
        {
            if (fixtures.Count == 0)
            {
                throw new Exception("Nėra nuskaitytų duomenų");
            }
            standartizedInputsData = new double[fixtures.Count][];
            standartizedInputsIntegerData = new int[fixtures.Count][];
            standartizedResultsData = new int[fixtures.Count];
            for (int i = 0; i < fixtures.Count; i++)
            {
                standartizedInputsData[i] = new double[fixtures[i].GetAttributesCount()];
                standartizedInputsIntegerData[i] = new int[fixtures[i].GetAttributesCount()];
                for (int j = 0; j < fixtures[i].GetAttributesCount(); j++)
                {
                    standartizedInputsData[i][j] = Convert.ToDouble(fixtures[i].GetAttributeValueAtIndex(j));
                    standartizedInputsIntegerData[i][j] = fixtures[i].GetAttributeValueAtIndex(j);
                }

                if (fixtures[i].fullTimeResult.Equals("H"))
                {
                    standartizedResultsData[i] = 0;
                }
                else if (fixtures[i].fullTimeResult.Equals("D"))
                {
                    standartizedResultsData[i] = 1;
                }
                else if (fixtures[i].fullTimeResult.Equals("A"))
                {
                    standartizedResultsData[i] = 2;
                }
            }
        }

        public void GetTestingTrainingData(int part, int partCount, double[][] testingInputs, int[] testingOutputs, double[][] trainingInputs, int[] trainingOutputs)
        {
            int testingIndex = 0;
            int trainingIndex = 0;
            int startRangeIndex = part * partCount;
            int endRangeIndex = (part * partCount) + partCount;
            for (int j = 0; j < standartizedInputsData.Length; j++)
            {
                if (j >= startRangeIndex && j < endRangeIndex)
                {
                    testingInputs[testingIndex] = new double[fixtures[j].GetAttributesCount()];
                    testingOutputs[testingIndex] = standartizedResultsData[j];
                    for (int l = 0; l < fixtures[j].GetAttributesCount(); l++)
                    {
                        testingInputs[testingIndex][l] = standartizedInputsData[j][l];
                    }
                    testingIndex++;
                }
                else
                {
                    trainingInputs[trainingIndex] = new double[fixtures[j].GetAttributesCount()];
                    trainingOutputs[trainingIndex] = standartizedResultsData[j];
                    for (int l = 0; l < fixtures[j].GetAttributesCount(); l++)
                    {
                        trainingInputs[trainingIndex][l] = standartizedInputsData[j][l];
                    }
                    trainingIndex++;
                }
            }
        }

        public int[] RandomForest(double[][] trainingInputs, int[] trainingOutputs, double[][] testingInputs)
        {
            var randomForestTeacher = new RandomForestLearning()
            {
                NumberOfTrees = 5,
            };
            var forest = randomForestTeacher.Learn(trainingInputs, trainingOutputs);
            return forest.Decide(testingInputs);
        }

        public int[] SVM(double[][] trainingInputs, int[] trainingOutputs, double[][] testingInputs)
        {
            var svmTeacher = new MulticlassSupportVectorLearning<Gaussian>()
            {
                Learner = (param) => new SequentialMinimalOptimization<Gaussian>()
                {
                    UseKernelEstimation = true
                }
            };

            svmTeacher.ParallelOptions.MaxDegreeOfParallelism = 1;
            var machine = svmTeacher.Learn(trainingInputs, trainingOutputs);
            return machine.Decide(testingInputs);
        }

        public int[] Bayes(double[][] trainingInputs, int[] trainingOutputs, double[][] testingInputs)
        {
            var learner = new NaiveBayesLearning<NormalDistribution, NormalOptions>();
            learner.Options.InnerOption.Regularization = 1e-5; // išvengti nulinių variacijų
            NaiveBayes<NormalDistribution> bayes = learner.Learn(trainingInputs, trainingOutputs);
            return bayes.Decide(testingInputs);
        }
    }
}
