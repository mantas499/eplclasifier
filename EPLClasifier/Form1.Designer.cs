﻿namespace EPLClasifier
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchButton = new System.Windows.Forms.Button();
            this.kaggleUsernameTextBox = new System.Windows.Forms.TextBox();
            this.datasetTextBox = new System.Windows.Forms.TextBox();
            this.kaggleUsernameLabel = new System.Windows.Forms.Label();
            this.datasetNameLabel = new System.Windows.Forms.Label();
            this.resultTextBox = new System.Windows.Forms.TextBox();
            this.downloadSetsButton = new System.Windows.Forms.Button();
            this.pcaButton = new System.Windows.Forms.Button();
            this.calcButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(128, 65);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(111, 46);
            this.searchButton.TabIndex = 0;
            this.searchButton.Text = "Ieškoti";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // kaggleUsernameTextBox
            // 
            this.kaggleUsernameTextBox.Location = new System.Drawing.Point(128, 13);
            this.kaggleUsernameTextBox.Name = "kaggleUsernameTextBox";
            this.kaggleUsernameTextBox.ReadOnly = true;
            this.kaggleUsernameTextBox.Size = new System.Drawing.Size(234, 20);
            this.kaggleUsernameTextBox.TabIndex = 1;
            this.kaggleUsernameTextBox.Text = "devinharia";
            // 
            // datasetTextBox
            // 
            this.datasetTextBox.Location = new System.Drawing.Point(128, 39);
            this.datasetTextBox.Name = "datasetTextBox";
            this.datasetTextBox.ReadOnly = true;
            this.datasetTextBox.Size = new System.Drawing.Size(234, 20);
            this.datasetTextBox.TabIndex = 2;
            this.datasetTextBox.Text = "epl-dataset";
            // 
            // kaggleUsernameLabel
            // 
            this.kaggleUsernameLabel.AutoSize = true;
            this.kaggleUsernameLabel.Location = new System.Drawing.Point(9, 16);
            this.kaggleUsernameLabel.Name = "kaggleUsernameLabel";
            this.kaggleUsernameLabel.Size = new System.Drawing.Size(89, 13);
            this.kaggleUsernameLabel.TabIndex = 3;
            this.kaggleUsernameLabel.Text = "Kaggle vartotojas";
            // 
            // datasetNameLabel
            // 
            this.datasetNameLabel.AutoSize = true;
            this.datasetNameLabel.Location = new System.Drawing.Point(9, 42);
            this.datasetNameLabel.Name = "datasetNameLabel";
            this.datasetNameLabel.Size = new System.Drawing.Size(113, 13);
            this.datasetNameLabel.TabIndex = 4;
            this.datasetNameLabel.Text = "Duomenų rinkinio pav.";
            // 
            // resultTextBox
            // 
            this.resultTextBox.Location = new System.Drawing.Point(12, 117);
            this.resultTextBox.Multiline = true;
            this.resultTextBox.Name = "resultTextBox";
            this.resultTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.resultTextBox.Size = new System.Drawing.Size(753, 298);
            this.resultTextBox.TabIndex = 5;
            // 
            // downloadSetsButton
            // 
            this.downloadSetsButton.Location = new System.Drawing.Point(245, 65);
            this.downloadSetsButton.Name = "downloadSetsButton";
            this.downloadSetsButton.Size = new System.Drawing.Size(117, 46);
            this.downloadSetsButton.TabIndex = 6;
            this.downloadSetsButton.Text = "Atsisiųsti";
            this.downloadSetsButton.UseVisualStyleBackColor = true;
            this.downloadSetsButton.Click += new System.EventHandler(this.downloadSetsButton_Click);
            // 
            // pcaButton
            // 
            this.pcaButton.Location = new System.Drawing.Point(380, 67);
            this.pcaButton.Name = "pcaButton";
            this.pcaButton.Size = new System.Drawing.Size(75, 43);
            this.pcaButton.TabIndex = 7;
            this.pcaButton.Text = "PCA analizė";
            this.pcaButton.UseVisualStyleBackColor = true;
            this.pcaButton.Click += new System.EventHandler(this.pcaButton_Click);
            // 
            // calcButton
            // 
            this.calcButton.Location = new System.Drawing.Point(461, 67);
            this.calcButton.Name = "calcButton";
            this.calcButton.Size = new System.Drawing.Size(75, 43);
            this.calcButton.TabIndex = 8;
            this.calcButton.Text = "Skaičiuoja";
            this.calcButton.UseVisualStyleBackColor = true;
            this.calcButton.Click += new System.EventHandler(this.calcButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 470);
            this.Controls.Add(this.calcButton);
            this.Controls.Add(this.pcaButton);
            this.Controls.Add(this.downloadSetsButton);
            this.Controls.Add(this.resultTextBox);
            this.Controls.Add(this.datasetNameLabel);
            this.Controls.Add(this.kaggleUsernameLabel);
            this.Controls.Add(this.datasetTextBox);
            this.Controls.Add(this.kaggleUsernameTextBox);
            this.Controls.Add(this.searchButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.TextBox kaggleUsernameTextBox;
        private System.Windows.Forms.TextBox datasetTextBox;
        private System.Windows.Forms.Label kaggleUsernameLabel;
        private System.Windows.Forms.Label datasetNameLabel;
        private System.Windows.Forms.TextBox resultTextBox;
        private System.Windows.Forms.Button downloadSetsButton;
        private System.Windows.Forms.Button pcaButton;
        private System.Windows.Forms.Button calcButton;
    }
}

