﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Accord.MachineLearning.Boosting;
using Accord.MachineLearning.Boosting.Learners;
using Accord.Statistics.Analysis;
using Accord.MachineLearning.Bayes;
using Accord.MachineLearning.Performance;
using Accord.MachineLearning.VectorMachines;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.Math.Optimization.Losses;
using Accord.MachineLearning;
using Accord.Statistics.Kernels;
using System.Diagnostics;
using Accord.MachineLearning.DecisionTrees;
using Accord.MachineLearning.DecisionTrees.Learning;
using Accord.Statistics.Models.Markov.Learning;
using Accord.Statistics.Models.Markov;
using Accord.Statistics.Models.Regression;
using Accord.Math;
using Accord.Statistics.Models.Regression.Fitting;

namespace EPLClasifier
{
    public partial class Form1 : Form
    {
        private KaggleService kaggleService = new KaggleService();
        private string kaggleUsername;
        private string kaggleDatasetName;
        private JArray datasetFiles;
        private Analysis analysis;

        public Form1()
        {
            InitializeComponent();
            analysis = new Analysis(resultTextBox);
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            kaggleUsername = kaggleUsernameTextBox.Text;
            kaggleDatasetName = datasetTextBox.Text;
            if (kaggleUsername.Equals("") || kaggleDatasetName.Equals(""))
            {
                resultTextBox.Text = "Vartotojas ir duomenų rinkinio pavadinimas privalomi";
                return;
            }
            JObject data = kaggleService.GetDatasets(kaggleUsername, kaggleDatasetName);
            datasetFiles = JArray.Parse(data["datasetFiles"].ToString());
            resultTextBox.Text = data.ToString();
        }

        private void downloadSetsButton_Click(object sender, EventArgs e)
        {
            if (kaggleUsername != null && kaggleDatasetName != null && datasetFiles.Count > 0)
            {
                resultTextBox.Text = "Parsisiunčiama";
                analysis.fixtures = kaggleService.GetDataset(kaggleUsername, kaggleDatasetName, datasetFiles[0]["ref"].ToString());
                resultTextBox.Text = "Parsiųsta";
                analysis.StandartizeData();
            }
        }

        private void pcaButton_Click(object sender, EventArgs e)
        {
            if (analysis.fixtures.Count > 0)
            {
                resultTextBox.Text = "PCA skaičiuojama";
                analysis.ComputePCA();
                resultTextBox.Text = "PCA suskaičiuota";
            }
        }

        private void calcButton_Click(object sender, EventArgs e)
        {
            analysis.Calculate();
        }
    }
}
