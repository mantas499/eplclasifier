﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace EPLClasifier
{
    class KaggleService
    {
        private const string kaggleJson = "../../kaggle.json";
        private const string baseApiUrl = "https://www.kaggle.com/api/v1/";
        private KaggleUserInfo kaggleUserInfo = new KaggleUserInfo();
        private HttpClient httpClient = new HttpClient();

        public KaggleService()
        {
            GetKaggleClient();
        }

        private void GetKaggleClient()
        {
            try
            {
                // nuskaitomas json failas ir priskiriamos reikšmės vartotojo objektui
                string json = File.ReadAllText(kaggleJson);
                JObject jObject = JObject.Parse(json);
                kaggleUserInfo = new KaggleUserInfo();
                kaggleUserInfo.username = jObject["username"].ToString();
                kaggleUserInfo.key = jObject["key"].ToString();
                // sugeneruojame token autorizacijai
                var authToken = Convert.ToBase64String(
                    System.Text.ASCIIEncoding.ASCII.GetBytes(
                        string.Format($"{kaggleUserInfo.username}:{kaggleUserInfo.key}")
                ));
                //priskiriama header reikšmė autorizacijai sudaryta iš username ir key reikšmių
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authToken);
            }
            catch(Exception exp)
            {
                Debug.WriteLine(exp.Message);
            }
        }

        public JObject GetDatasets(string user, string datasetName)
        {
            var response = httpClient.GetAsync(baseApiUrl + "datasets/list/" + user + "/" + datasetName).Result;
            var result = response.Content.ReadAsStringAsync().Result;
            JObject jObject = JObject.Parse(result);
            return jObject;
        }

        public List<Fixture> GetDataset(string user, string datasetName, string fileRef)
        {
            var response = httpClient.GetAsync(baseApiUrl + "datasets/download/" + user + "/" + datasetName + "/" + fileRef).Result;
            string result = response.Content.ReadAsStringAsync().Result;
            return ParseFixtureData(result);
        }

        private List<Fixture> ParseFixtureData(string result)
        {
            List<Fixture> fixtures = new List<Fixture>();
            string[] lines = result.Split('\n');
            string[] columnNames = lines.First().Split(',');
            lines = lines.Skip(1).ToArray();
            foreach(string line in lines)
            {
                if (!line.Equals(""))
                {
                    string[] parts = line.Split(',');
                    if (CheckForEmptyCells(parts))
                    {
                        continue;
                    }
                    Fixture fixture = new Fixture();
                    fixture.fixtureDate = parts[0];
                    fixture.homeTeam = parts[1];
                    fixture.awayTeam = parts[2];
                    for (int i = 3; i < parts.Length; i++)
                    {
                        if (i != 9 && i != parts.Length - 1 && i != 5 && i != 8)
                        {
                            KeyValuePair<string, int> attribute = new KeyValuePair<string, int>(columnNames[i], Convert.ToInt32(parts[i]));
                            fixture.AddAttribute(attribute);
                        }
                        else if (i == 5)
                        {
                            fixture.fullTimeResult = parts[i];
                        }
                        else if (i == 8)
                        {
                            fixture.halfTimeResult = parts[i];
                            int value = 0;
                            if (parts[i].Equals("D"))
                            {
                                value = 1;
                            }
                            else if (parts[i].Equals("A"))
                            {
                                value = 2;
                            }
                            KeyValuePair<string, int> attribute = new KeyValuePair<string, int>(columnNames[i], value);
                            fixture.AddAttribute(attribute);
                        }
                    }
                    fixtures.Add(fixture);
                }
            }
            return fixtures;
        }

        private bool CheckForEmptyCells(string[] parts)
        {
            bool empty = false;
            foreach(string part in parts)
            {
                if (part.Equals(""))
                {
                    empty = true;
                }
            }
            return empty;
        }

        public class KaggleUserInfo
        {
            public string username { get; set; }
            public string key { get; set; }
        }
    }
}
