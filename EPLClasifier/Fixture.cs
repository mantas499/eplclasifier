﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPLClasifier
{
    class Fixture
    {
        public string fixtureDate { get; set; }
        public string homeTeam { get; set; }
        public string awayTeam { get; set; }
        public string halfTimeResult { get; set; }
        public string fullTimeResult { get; set; }
        private List<KeyValuePair<string, int>> attributes = new List<KeyValuePair<string, int>>();

        public Fixture()
        {

        }

        public Fixture(string fixtureDate, string homeTeam, string awayTeam, List<KeyValuePair<string, int>> attributes)
        {
            this.fixtureDate = fixtureDate;
            this.homeTeam = homeTeam;
            this.awayTeam = awayTeam;
            this.attributes = attributes;
        }

        public void AddAttribute(KeyValuePair<string, int> attribute)
        {
            if (!attributes.Contains(attribute))
            {
                attributes.Add(attribute);
            }
        }

        public bool RemoveAttribute(KeyValuePair<string, int> attribute)
        {
            return attributes.Remove(attribute);
        }

        public int GetAttributesCount()
        {
            return attributes.Count;
        }

        public int GetAttributeValueAtIndex(int index)
        {
            return attributes[index].Value;
        }
    }
}
